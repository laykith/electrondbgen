import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/landing-page',
      name: 'landing-page',
      linkActiveClass: 'active',
      component: require('@/components/LandingPage').default
    },
    {
      path: '/',
      name: 'dashboard',
      linkActiveClass: 'active',
      component: require('@/components/Dashboard').default
    }
  ],
  linkActiveClass: "", // active class for non-exact links.
  linkExactActiveClass: "active" // active class for *exact* links.
})
